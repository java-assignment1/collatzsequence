package com.tw.collatzsequence;

import java.util.Scanner;

public class CollatzSequence {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter integer:");
        int number = scanner.nextInt();
        int count = 0;

        System.out.println(number);
        while (numberNotEqualsOne(number)) {
            if (isEven(number))
                number = number / 2;
            else
                number = 3 * (number) + 1;
            count++;
            System.out.println(number);
        }
        System.out.println(count);
    }

    private static boolean numberNotEqualsOne(int number) {
        return number != 1;
    }

    private static boolean isEven(int number) {
        return number % 2 == 0;
    }
}
